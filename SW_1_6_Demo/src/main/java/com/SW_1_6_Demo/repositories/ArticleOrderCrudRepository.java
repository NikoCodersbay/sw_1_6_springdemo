package com.SW_1_6_Demo.repositories;

import com.SW_1_6_Demo.entities.Article_Order;
import com.SW_1_6_Demo.entities.keys.Article_order_PK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleOrderCrudRepository extends JpaRepository<Article_Order, Article_order_PK> {
}
