package com.SW_1_6_Demo.repositories;

import com.SW_1_6_Demo.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderCrudRepository extends JpaRepository<Order,Integer> {
}
