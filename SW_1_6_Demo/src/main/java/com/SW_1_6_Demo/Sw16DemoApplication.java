package com.SW_1_6_Demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sw16DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sw16DemoApplication.class, args);
	}

}
