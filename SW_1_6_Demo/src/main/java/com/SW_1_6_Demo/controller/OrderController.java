package com.SW_1_6_Demo.controller;

import com.SW_1_6_Demo.customException.UserIsLOckedException;
import com.SW_1_6_Demo.customException.UserNotFoundException;
import com.SW_1_6_Demo.dtos.OrderDTO;
import com.SW_1_6_Demo.dtos.ResponseOrderDTO;
import com.SW_1_6_Demo.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping
    private ResponseEntity<?> createOrder(@RequestBody OrderDTO orderDTO){
        ResponseOrderDTO responseOrderDTO;
        try {
             responseOrderDTO = orderService.createOrder(orderDTO);
        }catch (UserNotFoundException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (UserIsLOckedException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(responseOrderDTO, HttpStatus.CREATED);
    }


}
