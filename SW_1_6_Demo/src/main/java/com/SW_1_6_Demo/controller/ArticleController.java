package com.SW_1_6_Demo.controller;

import com.SW_1_6_Demo.dtos.ArticleDto;
import com.SW_1_6_Demo.dtos.ResponseArticleDto;
import com.SW_1_6_Demo.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("api/article")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @PostMapping
    public ResponseEntity<?> createArticle(@RequestBody ArticleDto articleDto){
        ResponseArticleDto responseArticleDto;
        try{
           responseArticleDto = articleService.createArticle(articleDto);
        }catch (NoSuchElementException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }catch (RuntimeException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage() ,HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(responseArticleDto,HttpStatus.CREATED);
    }
}
