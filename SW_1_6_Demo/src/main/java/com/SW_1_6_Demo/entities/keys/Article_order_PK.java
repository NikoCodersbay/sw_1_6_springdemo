package com.SW_1_6_Demo.entities.keys;

import com.SW_1_6_Demo.entities.Article;
import com.SW_1_6_Demo.entities.Order;
import lombok.*;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Article_order_PK implements Serializable {
    private Article article;

    private Order order;
}
