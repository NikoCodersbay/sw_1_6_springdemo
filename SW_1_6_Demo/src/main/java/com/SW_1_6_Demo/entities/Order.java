package com.SW_1_6_Demo.entities;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Entity(name = "Orders")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderNumber;

    private LocalDate orderDate;

    @ManyToOne
    @JoinColumn()
    private User orderingUser;

    @OneToMany(mappedBy = "order")
    private List<Article_Order> articleOrderList;
}
