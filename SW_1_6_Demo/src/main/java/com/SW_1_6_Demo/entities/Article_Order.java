package com.SW_1_6_Demo.entities;


import com.SW_1_6_Demo.entities.keys.Article_order_PK;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@IdClass(Article_order_PK.class)
public class Article_Order {

    @Id
    @ManyToOne
    @JoinColumn(name = "articleCode_order")
    private Article article;

    @Id
    @ManyToOne
    @JoinColumn(name = "orderNumber_order")
    private Order order;

    @Column(nullable = false)
    private int qty;

    private double sum;
}
