package com.SW_1_6_Demo.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String articleCode;

    private String articleName;

    @Column(nullable = false)
    private double price;

    private double weight;

    @ManyToMany
    @JoinTable(
        name = "Article_ArticleCategory",
        joinColumns = @JoinColumn(name = "categoryId"),
        inverseJoinColumns = @JoinColumn(name = "articleCode")
    )
    private List<ArticleCategory> articleCategoryList;

    @OneToMany(mappedBy = "article")
    private List<Article_Order> articleOrderList;

}
