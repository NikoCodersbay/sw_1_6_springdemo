package com.SW_1_6_Demo.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity(name = "Users")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;

    private String username;
    private String eMail;
    private String password;
    private boolean locked;

    @OneToMany(mappedBy = "orderingUser")
    private List<Order> orderList;
}
