package com.SW_1_6_Demo.dtos;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseOrderDTO {

    private int orderNumber;
    private String orderDate;
    private int userId;
    List<ResponseArticleOrderDTO> responseArticleOrderDTOList = new ArrayList<>();
}
