package com.SW_1_6_Demo.dtos;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseArticleDto {

    private String articleCode;
    private String articleName;
    private double price;
    private double weight;
    private List<Integer> articleCategoryIdList = new ArrayList<>();

}
