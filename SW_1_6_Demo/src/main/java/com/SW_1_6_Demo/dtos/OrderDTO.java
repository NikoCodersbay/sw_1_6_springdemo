package com.SW_1_6_Demo.dtos;

import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrderDTO {

    private String orderDate;
    private int userId;
    private List<Article_OrderDto> articleOrderDtoList = new ArrayList<>();

}
