package com.SW_1_6_Demo.dtos;


import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseArticleOrderDTO {

    private String articleNumber;
    private int qty;
    private double sum;
}
