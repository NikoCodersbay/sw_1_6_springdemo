package com.SW_1_6_Demo.dtos;


import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Article_OrderDto {

    private String articleNumber;
    private int qty;
}
