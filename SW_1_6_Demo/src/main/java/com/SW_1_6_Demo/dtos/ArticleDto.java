package com.SW_1_6_Demo.dtos;

import com.SW_1_6_Demo.entities.ArticleCategory;
import jdk.jfr.Category;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ArticleDto {

    private String articleName;
    private double price;
    private double weight;
    private List<Integer> articleCategoryIdList = new ArrayList<>();
}
