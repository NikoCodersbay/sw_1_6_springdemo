package com.SW_1_6_Demo.services;

import com.SW_1_6_Demo.customException.UserIsLOckedException;
import com.SW_1_6_Demo.customException.UserNotFoundException;
import com.SW_1_6_Demo.dtos.OrderDTO;
import com.SW_1_6_Demo.dtos.ResponseOrderDTO;
import com.SW_1_6_Demo.entities.Order;
import com.SW_1_6_Demo.entities.User;
import com.SW_1_6_Demo.repositories.OrderCrudRepository;
import com.SW_1_6_Demo.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    UserCrudRepository userCrudRepository;

    @Autowired
    OrderCrudRepository orderCrudRepository;

    public ResponseOrderDTO createOrder(OrderDTO orderDTO){

        Optional<User> userOptional = userCrudRepository.findById(orderDTO.getUserId());

        if (userOptional.isEmpty()){
            throw new UserNotFoundException("Es gibt keinen USer mit der ID: " + orderDTO.getUserId());
        }

        User user = userOptional.get();

        if (user.isLocked()){
            throw new UserIsLOckedException("User is locked!!!!!!!!!!");
        }

        Order order = Order.builder()
                        .orderDate(LocalDate.parse(orderDTO.getOrderDate()))
                        .orderingUser(user)
                        .build();


        orderCrudRepository.save(order);

        return new ResponseOrderDTO();
    }
}
