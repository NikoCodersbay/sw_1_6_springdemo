package com.SW_1_6_Demo.services;

import com.SW_1_6_Demo.dtos.ArticleDto;
import com.SW_1_6_Demo.dtos.ResponseArticleDto;
import com.SW_1_6_Demo.dtos.ResponseArticleOrderDTO;
import com.SW_1_6_Demo.entities.Article;
import com.SW_1_6_Demo.entities.ArticleCategory;
import com.SW_1_6_Demo.repositories.ArticleCategoryCrudRepository;
import com.SW_1_6_Demo.repositories.ArticleCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ArticleService {
    @Autowired
    ArticleCategoryCrudRepository articleCategoryCrudRepository;
    @Autowired
    ArticleCrudRepository articleCrudRepository;

    public ResponseArticleDto createArticle(ArticleDto articleDto) {
        Article article = Article.builder()
                .articleName(articleDto.getArticleName())
                .price(articleDto.getPrice())
                .weight(articleDto.getWeight())
                .articleCategoryList(new ArrayList<>())
                .build();
        for (Integer i : articleDto.getArticleCategoryIdList()) {
            Optional<ArticleCategory> articleCategoryOptional = articleCategoryCrudRepository.findById(i);
            if (articleCategoryOptional.isEmpty()) {
                throw new NoSuchElementException("Keine Kategorie mit der Id :" + i + " gefunden!");
            }
            ArticleCategory articleCategory = articleCategoryOptional.get();
            article.getArticleCategoryList().add(articleCategory);
        }
        articleCrudRepository.save(article);
        List<Integer> categoryIdList = new ArrayList<>();
        for (ArticleCategory a : article.getArticleCategoryList()){
            categoryIdList.add(a.getCategoryId());
        }
        ResponseArticleDto responseArticleDto= new ResponseArticleDto(article.getArticleCode(),article.getArticleName(), articleDto.getPrice(), articleDto.getWeight(), categoryIdList);
        return responseArticleDto;
    }
}
