package com.SW_1_6_Demo.customException;

public class UserIsLOckedException extends RuntimeException{

    public UserIsLOckedException(String message) {
        super(message);
    }
}
